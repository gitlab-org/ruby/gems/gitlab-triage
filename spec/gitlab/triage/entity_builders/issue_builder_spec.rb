# frozen_string_literal: true

require 'spec_helper'

require 'gitlab/triage/entity_builders/issue_builder'

describe Gitlab::Triage::EntityBuilders::IssueBuilder do
  include_context 'with network context'

  let(:resource) do
    { title: 'Issue #0', web_url: 'http://example.com/0', references: { full: 'project/id#123' } }
  end

  let(:rule) do
    {
      title: title,
      description: description
    }
  end

  let(:title) { 'Issue title for {{title}}' }
  let(:expected_title) { 'Issue title for Issue #0' }
  let(:description) { '\# {{title}}\n\nHere\'s the issue:\n\n{{full_reference}}' }

  subject do
    described_class.new(
      type: 'issues',
      action: rule,
      resource: resource,
      network: network)
  end

  describe '#title' do
    it 'generates the correct issue title' do
      expect(subject.title).to eq(expected_title)
    end
  end

  describe '#description' do
    it 'generates the correct issue description' do
      expect(subject.description).to eq(<<~TEXT.chomp)
        # Issue #0

        Here's the issue:

        project/id#123
      TEXT
    end

    context 'when there is no description' do
      let(:description) {}

      it 'generates an empty description' do
        expect(subject.description).to be_empty
      end
    end
  end

  describe '#valid?' do
    context 'when title is nil' do
      let(:title) {}

      it 'is not valid' do
        expect(subject).not_to be_valid
      end
    end

    context 'when title is blank' do
      let(:title) { '  ' }

      it 'is not valid' do
        expect(subject).not_to be_valid
      end
    end

    context 'when title is not blank' do
      let(:title) { 'title' }

      it 'is valid' do
        expect(subject).to be_valid
      end
    end
  end
end
