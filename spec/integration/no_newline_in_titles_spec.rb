# frozen_string_literal: true

require 'spec_helper'

describe 'Translate newlines to spaces in titles' do
  include_context 'with integration context'

  before do
    issue[:title] = "issue\r\ntitle"
  end

  def stub_and_perform_summary(rule, expected_title, expected_description)
    stub_post = stub_api(
      :post,
      "https://gitlab.com/api/v4/projects/#{project_id}/issues",
      body: { title: expected_title, description: expected_description },
      headers: { 'PRIVATE-TOKEN' => token })

    perform(rule)

    assert_requested(stub_post)
  end

  shared_examples 'creating a summary issue' do
    it 'creates a summary issue with translated titles' do
      expected_title = 'Summary for issues'
      expected_description = <<~MARKDOWN.chomp
      Start summary

      - issue  title

      End summary
      MARKDOWN

      stub_and_perform_summary(rule, expected_title, h(expected_description))
    end
  end

  context 'with REST API' do
    let(:rule) do
      <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Rule name
                actions:
                  summarize:
                    item: |
                      - {{title}}
                    title: |
                      Summary for \#{resource[:type]}
                    summary: |
                      Start summary

                      {{items}}

                      End summary
      YAML
    end

    before do
      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}/issues",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => token }) do
        issues
      end
    end

    it_behaves_like 'creating a summary issue'
  end

  context 'with GraphQL' do
    let(:rule) do
      <<~YAML
        resource_rules:
          issues:
            rules:
              - name: Rule name
                api: graphql
                actions:
                  summarize:
                    item: |
                      - {{title}}
                    title: |
                      Summary for \#{resource[:type]}
                    summary: |
                      Start summary

                      {{items}}

                      End summary
      YAML
    end

    before do
      issue[:id] = "gid://gitlab/Issue/#{issue[:id]}"
      issue[:labels] = { 'nodes' => [] }
      issue[:assignees] = { 'nodes' => [] }

      stub_graphql_introspection

      stub_api(
        :get,
        "https://gitlab.com/api/v4/projects/#{project_id}",
        query: { per_page: 100 },
        headers: { 'PRIVATE-TOKEN' => token }) do
        { 'full_path' => 'namespace/project' }
      end

      stub_api(
        :post,
        'https://gitlab.com/api/graphql',
        body: /project\(fullPath: \$source\)/) do
        { 'data' => { 'project' => { 'issues' => { 'pageInfo' => { 'hasNextPage' => false, 'endCursor' => true }, 'nodes' => issues } } } }
      end
    end

    it_behaves_like 'creating a summary issue'
  end
end
